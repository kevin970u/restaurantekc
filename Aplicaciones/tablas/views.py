from django.shortcuts import render, redirect
from .models import Provincia, Cliente, Pedido, Tipo, Platillo, Detalle, Ingrediente, Receta
from django.contrib import messages

# Create your views here.
def listadoProvincia(request):
	provinciaBdd = Provincia.objects.all()
	return render(request, 'listadoProvincia.html', {'provincia': provinciaBdd})

def guardarProvincia(request):
	nombreprovincia_kc = request.POST["nombreprovincia_kc"]
	codigopostalprovincia_kc = request.POST["codigopostalprovincia_kc"]
	regionprovincia_kc = request.POST["regionprovincia_kc"]

	nuevoProvincia = Provincia.objects.create(nombreprovincia_kc=nombreprovincia_kc, codigopostalprovincia_kc=codigopostalprovincia_kc, regionprovincia_kc=regionprovincia_kc)
	messages.success(request, 'Provincia guardada Exitosamente')
	return redirect('/')

def eliminarProvincia(request, idprovincia_kc):
	provinciaEliminar = Provincia.objects.get(idprovincia_kc=idprovincia_kc)
	provinciaEliminar.delete()
	messages.success(request, 'Provincia eliminada Exitosamente')
	return redirect('/')

def editarProvincia(request, idprovincia_kc):
	provinciaEditar = Provincia.objects.get(idprovincia_kc=idprovincia_kc)
	return render(request, 'editarProvincia.html', {'provincia': provinciaEditar})

def procesarActualizacionProvincia(request):
	idprovincia_kc = request.POST["idprovincia_kc"]
	nombreprovincia_kc = request.POST["nombreprovincia_kc"]
	codigopostalprovincia_kc = request.POST["codigopostalprovincia_kc"]
	regionprovincia_kc = request.POST["regionprovincia_kc"]

	provinciaEditar = Provincia.objects.get(idprovincia_kc=idprovincia_kc)
	provinciaEditar.nombreprovincia_kc = nombreprovincia_kc
	provinciaEditar.codigopostalprovincia_kc = codigopostalprovincia_kc
	provinciaEditar.regionprovincia_kc = regionprovincia_kc
	provinciaEditar.save()
	messages.success(request, 'Provincia actualizada Exitosamente')
	return redirect('/')


def listadoCliente(request):
	clienteBdd = Cliente.objects.all()
	provinciaBdd = Provincia.objects.all()
	return render(request, 'listadoCliente.html', {'cliente': clienteBdd, 'provincia': provinciaBdd})

def guardarCliente(request):
	idprovincia_kc = request.POST["idprovincia_kc"]
	provinciaSeleccionado = Provincia.objects.get(idprovincia_kc=idprovincia_kc)
	cedulacliente_kc = request.POST["cedulacliente_kc"]
	apellidocliente_kc = request.POST["apellidocliente_kc"]
	nombrecliente_kc = request.POST["nombrecliente_kc"]
	fotografiacliente_kc = request.FILES.get("fotografiacliente_kc")

	nuevoCliente = Cliente.objects.create(cedulacliente_kc=cedulacliente_kc, apellidocliente_kc=apellidocliente_kc, nombrecliente_kc=nombrecliente_kc, provincia=provinciaSeleccionado, fotografiacliente_kc=fotografiacliente_kc)
	messages.success(request, 'Cliente guardado Exitosamente')
	return redirect('/')

def eliminarCliente(request, idcliente_kc):
	clienteEliminar = Cliente.objects.get(idcliente_kc=idcliente_kc)
	clienteEliminar.delete()
	messages.success(request, 'Cliente eliminado Exitosamente')
	return redirect('/')

def editarCliente(request, idcliente_kc):
	clienteEditar = Cliente.objects.get(idcliente_kc=idcliente_kc)
	provinciaBdd = Provincia.objects.all()
	return render(request, 'editarCliente.html', {'cliente': clienteEditar, 'provincia': provinciaBdd})

def procesarActualizacionCliente(request):
	idcliente_kc = request.POST["idcliente_kc"]
	idprovincia_kc = request.POST["idprovincia_kc"]
	provinciaSeleccionado = Provincia.objects.get(idprovincia_kc=idprovincia_kc)
	cedulacliente_kc = request.POST["cedulacliente_kc"]
	apellidocliente_kc = request.POST["apellidocliente_kc"]
	nombrecliente_kc = request.POST["nombrecliente_kc"]
	fotografiacliente_kc = request.FILES.get("fotografiacliente_kc")

	clienteEditar = Cliente.objects.get(idcliente_kc=idcliente_kc)
	clienteEditar.provincia = provinciaSeleccionado
	clienteEditar.apellidocliente_kc = apellidocliente_kc
	clienteEditar.nombrecliente_kc = nombrecliente_kc
	clienteEditar.fotografiacliente_kc = fotografiacliente_kc
	clienteEditar.save()
	messages.success(request, 'Cliente actualizado Exitosamente')
	return redirect('/')


def listadoPedido(request):
	pedidoBdd = Pedido.objects.all()
	clienteBdd = Cliente.objects.all()
	return render(request, 'listadoPedido.html', {'pedido': pedidoBdd, 'cliente': clienteBdd})

def guardarPedido(request):
	idcliente_kc = request.POST["idcliente_kc"]
	clienteSeleccionado = Cliente.objects.get(idcliente_kc=idcliente_kc)
	fechaemisionpedido_kc = request.POST["fechaemisionpedido_kc"]
	fechavencimientopedido_kc = request.POST["fechavencimientopedido_kc"]

	nuevoPedido = Pedido.objects.create(fechaemisionpedido_kc=fechaemisionpedido_kc, fechavencimientopedido_kc=fechavencimientopedido_kc, cliente=clienteSeleccionado)
	messages.success(request, 'Pedido guardado Exitosamente')
	return redirect('/')

def eliminarPedido(request, idpedido_kc):
	pedidoEliminar = Pedido.objects.get(idpedido_kc=idpedido_kc)
	pedidoEliminar.delete()
	messages.success(request, 'Pedido eliminado Exitosamente')
	return redirect('/')

def editarPedido(request, idpedido_kc):
	pedidoEditar = Pedido.objects.get(idpedido_kc=idpedido_kc)
	clienteBdd = Cliente.objects.all()
	return render(request, 'editarPedido.html', {'pedido': pedidoEditar, 'cliente': clienteBdd})

def procesarActualizacionPedido(request):
	idpedido_kc = request.POST["idpedido_kc"]
	idcliente_kc = request.POST["idcliente_kc"]
	clienteSeleccionado = Cliente.objects.get(idcliente_kc=idcliente_kc)
	fechaemisionpedido_kc = request.POST["fechaemisionpedido_kc"]
	fechavencimientopedido_kc = request.POST["fechavencimientopedido_kc"]

	pedidoEditar = Pedido.objects.get(idpedido_kc=idpedido_kc)
	pedidoEditar.cliente = clienteSeleccionado
	pedidoEditar.fechaemisionpedido_kc = fechaemisionpedido_kc
	pedidoEditar.fechavencimientopedido_kc = fechavencimientopedido_kc
	pedidoEditar.save()
	messages.success(request, 'Pedido actualizado Exitosamente')
	return redirect('/')


def listadoTipo(request):
	tipoBdd = Tipo.objects.all()
	return render(request, 'listadoTipo.html', {'tipo': tipoBdd})

def guardarTipo(request):
	categoriatipo_kc = request.POST["categoriatipo_kc"]
	descripciontipo_kc = request.POST["descripciontipo_kc"]
	fotografiatipo_kc = request.FILES.get("fotografiatipo_kc")

	nuevoTipo = Tipo.objects.create(categoriatipo_kc=categoriatipo_kc, descripciontipo_kc=descripciontipo_kc, fotografiatipo_kc=fotografiatipo_kc)
	messages.success(request, 'Tipo guardado Exitosamente')
	return redirect('/')

def eliminarTipo(request, idtipo_kc):
	tipoEliminar = Tipo.objects.get(idtipo_kc=idtipo_kc)
	tipoEliminar.delete()
	messages.success(request, 'Tipo eliminado Exitosamente')
	return redirect('/')

def editarTipo(request, idtipo_kc):
	tipoEditar = Tipo.objects.get(idtipo_kc=idtipo_kc)
	return render(request, 'editarTipo.html', {'tipo': tipoEditar})

def procesarActualizacionTipo(request):
	idtipo_kc = request.POST["idtipo_kc"]
	categoriatipo_kc = request.POST["categoriatipo_kc"]
	descripciontipo_kc = request.POST["descripciontipo_kc"]
	fotografiatipo_kc = request.FILES.get("fotografiatipo_kc")

	tipoEditar = Tipo.objects.get(idtipo_kc=idtipo_kc)
	tipoEditar.categoriatipo_kc = categoriatipo_kc
	tipoEditar.descripciontipo_kc = descripciontipo_kc
	tipoEditar.fotografiatipo_kc = fotografiatipo_kc
	tipoEditar.save()
	messages.success(request, 'Tipo actualizado Exitosamente')
	return redirect('/')


def listadoPlatillo(request):
	platilloBdd = Platillo.objects.all()
	tipoBdd = Tipo.objects.all()
	return render(request, 'listadoPlatillo.html', {'platillo': platilloBdd, 'tipo': tipoBdd})

def guardarPlatillo(request):
	idtipo_kc = request.POST["idtipo_kc"]
	tipoSeleccionado = Tipo.objects.get(idtipo_kc=idtipo_kc)
	nombreplatillo_kc = request.POST["nombreplatillo_kc"]
	descripcionplatillo_kc = request.POST["descripcionplatillo_kc"]
	precioplatillo_kc = request.POST["precioplatillo_kc"]
	fotografiaplatillo_kc = request.FILES.get("fotografiaplatillo_kc")

	nuevoPlatillo = Platillo.objects.create(nombreplatillo_kc=nombreplatillo_kc, descripcionplatillo_kc=descripcionplatillo_kc, precioplatillo_kc=precioplatillo_kc, fotografiaplatillo_kc=fotografiaplatillo_kc, tipo=tipoSeleccionado)
	messages.success(request, 'Platillo guardado Exitosamente')
	return redirect('/')

def eliminarPlatillo(request, idplatillo_kc):
	platilloEliminar = Platillo.objects.get(idplatillo_kc=idplatillo_kc)
	platilloEliminar.delete()
	messages.success(request, 'Platillo eliminado Exitosamente')
	return redirect('/')

def editarPlatillo(request, idplatillo_kc):
	platilloEditar = Platillo.objects.get(idplatillo_kc=idplatillo_kc)
	tipoBdd = Tipo.objects.all()
	return render(request, 'editarPlatillo.html', {'platillo': platilloEditar, 'tipo': tipoBdd})

def procesarActualizacionPlatillo(request):
	idplatillo_kc = request.POST["idplatillo_kc"]
	idtipo_kc = request.POST["idtipo_kc"]
	tipoSeleccionado = Tipo.objects.get(idtipo_kc=idtipo_kc)
	nombreplatillo_kc = request.POST["nombreplatillo_kc"]
	descripcionplatillo_kc = request.POST["descripcionplatillo_kc"]
	precioplatillo_kc = request.POST["precioplatillo_kc"]
	fotografiaplatillo_kc = request.FILES.get("fotografiaplatillo_kc")

	platilloEditar = Platillo.objects.get(idplatillo_kc=idplatillo_kc)
	platilloEditar.tipo = tipoSeleccionado
	platilloEditar.nombreplatillo_kc = nombreplatillo_kc
	platilloEditar.descripcionplatillo_kc = descripcionplatillo_kc
	platilloEditar.precioplatillo_kc = precioplatillo_kc
	platilloEditar.fotografiaplatillo_kc = fotografiaplatillo_kc
	platilloEditar.save()
	messages.success(request, 'Platillo actualizado Exitosamente')
	return redirect('/')



def listadoDetalle(request):
	detalleBdd = Detalle.objects.all()
	pedidoBdd = Pedido.objects.all()
	platilloBdd = Platillo.objects.all()
	return render(request, 'listadoDetalle.html', {'detalle': detalleBdd, 'pedido': pedidoBdd, 'platillo': platilloBdd})

def guardarDetalle(request):
	idpedido_kc = request.POST["idpedido_kc"]
	pedidoSeleccionado = Pedido.objects.get(idpedido_kc=idpedido_kc)
	idplatillo_kc = request.POST["idplatillo_kc"]
	platilloSeleccionado = Platillo.objects.get(idplatillo_kc=idplatillo_kc)
	descripciondetalle_kc = request.POST["descripciondetalle_kc"]
	cantidaddetalle_kc = request.POST["cantidaddetalle_kc"]

	nuevoDetalle = Detalle.objects.create(descripciondetalle_kc=descripciondetalle_kc, cantidaddetalle_kc=cantidaddetalle_kc, pedido=pedidoSeleccionado, platillo=platilloSeleccionado)
	messages.success(request, 'Detalle guardado Exitosamente')
	return redirect('/')

def eliminarDetalle(request, iddetalle_kc):
	detalleEliminar = Detalle.objects.get(iddetalle_kc=iddetalle_kc)
	detalleEliminar.delete()
	messages.success(request, 'Detalle eliminado Exitosamente')
	return redirect('/')

def editarDetalle(request, iddetalle_kc):
	detalleEditar = Detalle.objects.get(iddetalle_kc=iddetalle_kc)
	pedidoBdd = Pedido.objects.all()
	platilloBdd = Platillo.objects.all()
	return render(request, 'editarDetalle.html', {'detalle': detalleEditar, 'pedido': pedidoBdd, 'platillo': platilloBdd})

def procesarActualizacionDetalle(request):
	iddetalle_kc = request.POST["iddetalle_kc"]
	idpedido_kc = request.POST["idpedido_kc"]
	pedidoSeleccionado = Pedido.objects.get(idpedido_kc=idpedido_kc)
	idplatillo_kc = request.POST["idplatillo_kc"]
	platilloSeleccionado = Platillo.objects.get(idplatillo_kc=idplatillo_kc)
	descripciondetalle_kc = request.POST["descripciondetalle_kc"]
	cantidaddetalle_kc = request.POST["cantidaddetalle_kc"]


	detalleEditar = Detalle.objects.get(iddetalle_kc=iddetalle_kc)
	detalleEditar.pedido = pedidoSeleccionado
	detalleEditar.platillo = platilloSeleccionado
	detalleEditar.descripciondetalle_kc = descripciondetalle_kc
	detalleEditar.cantidaddetalle_kc = cantidaddetalle_kc
	detalleEditar.save()
	messages.success(request, 'Detalle actualizado Exitosamente')
	return redirect('/')



def listadoIngrediente(request):
	ingredienteBdd = Ingrediente.objects.all()
	return render(request, 'listadoIngrediente.html', {'ingrediente': ingredienteBdd})

def guardarIngrediente(request):
	nombreingrediente_kc = request.POST["nombreingrediente_kc"]
	fechaexpiracioningrediente_kc = request.POST["fechaexpiracioningrediente_kc"]
	fotografiaingrediente_kc = request.FILES.get("fotografiaingrediente_kc")

	nuevoIngrediente = Ingrediente.objects.create(nombreingrediente_kc=nombreingrediente_kc, fechaexpiracioningrediente_kc=fechaexpiracioningrediente_kc, fotografiaingrediente_kc=fotografiaingrediente_kc)
	messages.success(request, 'Ingrediente guardado Exitosamente')
	return redirect('/')

def eliminarIngrediente(request, idingrediente_kc):
	ingredienteEliminar = Ingrediente.objects.get(idingrediente_kc=idingrediente_kc)
	ingredienteEliminar.delete()
	messages.success(request, 'Ingrediente eliminado Exitosamente')
	return redirect('/')

def editarIngrediente(request, idingrediente_kc):
	ingredienteEditar = Ingrediente.objects.get(idingrediente_kc=idingrediente_kc)
	return render(request, 'editarIngrediente.html', {'ingrediente': ingredienteEditar})

def procesarActualizacionIngrediente(request):
	idingrediente_kc = request.POST["idingrediente_kc"]
	nombreingrediente_kc = request.POST["nombreingrediente_kc"]
	fechaexpiracioningrediente_kc = request.POST["fechaexpiracioningrediente_kc"]
	fotografiaingrediente_kc = request.FILES.get("fotografiaingrediente_kc")


	ingredienteEditar = Ingrediente.objects.get(idingrediente_kc=idingrediente_kc)
	ingredienteEditar.nombreingrediente_kc = nombreingrediente_kc
	ingredienteEditar.fechaexpiracioningrediente_kc = fechaexpiracioningrediente_kc
	ingredienteEditar.fotografiaingrediente_kc = fotografiaingrediente_kc
	ingredienteEditar.save()
	messages.success(request, 'Ingrediente actualizado Exitosamente')
	return redirect('/')



def listadoReceta(request):
	recetaBdd = Receta.objects.all()
	ingredienteBdd = Ingrediente.objects.all()
	platilloBdd = Platillo.objects.all()
	return render(request, 'listadoReceta.html', {'receta': recetaBdd, 'ingrediente': ingredienteBdd, 'platillo': platilloBdd})

def guardarReceta(request):
	idingrediente_kc = request.POST["idingrediente_kc"]
	ingredienteSeleccionado = Ingrediente.objects.get(idingrediente_kc=idingrediente_kc)
	idplatillo_kc = request.POST["idplatillo_kc"]
	platilloSeleccionado = Platillo.objects.get(idplatillo_kc=idplatillo_kc)
	descripcionreceta_kc = request.POST["descripcionreceta_kc"]
	dificultadreceta_kc = request.POST["dificultadreceta_kc"]

	nuevoReceta = Receta.objects.create(descripcionreceta_kc=descripcionreceta_kc, dificultadreceta_kc=dificultadreceta_kc, ingrediente=ingredienteSeleccionado, platillo=platilloSeleccionado)
	messages.success(request, 'Detalle guardado Exitosamente')
	return redirect('/')

def eliminarReceta(request, idreceta_kc):
	recetaEliminar = Receta.objects.get(idreceta_kc=idreceta_kc)
	recetaEliminar.delete()
	messages.success(request, 'Receta eliminada Exitosamente')
	return redirect('/')

def editarReceta(request, idreceta_kc):
	recetaEditar = Receta.objects.get(idreceta_kc=idreceta_kc)
	ingredienteBdd = Ingrediente.objects.all()
	platilloBdd = Platillo.objects.all()
	return render(request, 'editarReceta.html', {'receta': recetaEditar, 'ingrediente': ingredienteBdd, 'platillo': platilloBdd})

def procesarActualizacionReceta(request):
	idreceta_kc = request.POST["idreceta_kc"]
	idingrediente_kc = request.POST["idingrediente_kc"]
	ingredienteSeleccionado = Ingrediente.objects.get(idingrediente_kc=idingrediente_kc)
	idplatillo_kc = request.POST["idplatillo_kc"]
	platilloSeleccionado = Platillo.objects.get(idplatillo_kc=idplatillo_kc)
	descripcionreceta_kc = request.POST["descripcionreceta_kc"]
	dificultadreceta_kc = request.POST["dificultadreceta_kc"]


	recetaEditar = Receta.objects.get(idreceta_kc=idreceta_kc)
	recetaEditar.ingrediente = ingredienteSeleccionado
	recetaEditar.platillo = platilloSeleccionado
	recetaEditar.descripcionreceta_kc = descripcionreceta_kc
	recetaEditar.dificultadreceta_kc = dificultadreceta_kc
	recetaEditar.save()
	messages.success(request, 'Receta actualizada Exitosamente')
	return redirect('/')