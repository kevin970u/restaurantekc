from django.db import models

# Create your models here.

class Provincia(models.Model):
    idprovincia_kc = models.AutoField(primary_key=True)
    nombreprovincia_kc = models.CharField(max_length=150)
    codigopostalprovincia_kc = models.IntegerField()
    regionprovincia_kc = models.CharField(max_length=150)

    def __str__(self):
        fila = "{0}: {1} {2}"
        return fila.format(self.nombreprovincia_kc, self.codigopostalprovincia_kc, self.regionprovincia_kc)


class Cliente(models.Model):
    idcliente_kc = models.AutoField(primary_key=True)
    cedulacliente_kc = models.CharField(max_length=150)
    apellidocliente_kc = models.CharField(max_length=150)
    nombrecliente_kc = models.CharField(max_length=150)
    fotografiacliente_kc = models.FileField(upload_to='clientes',null=True,blank=True)
    provincia = models.ForeignKey(Provincia,null=True,blank=True,on_delete=models.PROTECT)
    
    def __str__(self):
        fila = "{0}: {1} {2}"
        return fila.format(self.cedulacliente_kc,self.apellidocliente_kc,self.nombrecliente_kc)


class Pedido(models.Model):
    idpedido_kc = models.AutoField(primary_key=True)
    fechaemisionpedido_kc = models.DateField()
    fechavencimientopedido_kc = models.DateField()
    cliente = models.ForeignKey(Cliente, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        fila = "{0}: {1}"
        return fila.format(self.fechaemisionpedido_kc, self.fechavencimientopedido_kc)


class Tipo(models.Model):
    idtipo_kc = models.AutoField(primary_key=True)
    categoriatipo_kc = models.CharField(max_length=150)
    descripciontipo_kc = models.TextField()
    fotografiatipo_kc = models.FileField(upload_to='tipos', null=True, blank=True)

    def __str__(self):
        fila = "{0}: {1}"
        return fila.format(self.categoriatipo_kc, self.descripciontipo_kc)


class Platillo(models.Model):
    idplatillo_kc = models.AutoField(primary_key=True)
    nombreplatillo_kc = models.CharField(max_length=150)
    descripcionplatillo_kc = models.TextField()
    precioplatillo_kc = models.FloatField()
    fotografiaplatillo_kc = models.FileField(upload_to='platillos', null=True, blank=True)
    tipo = models.ForeignKey(Tipo, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        fila = "{0}: {1} {2}"
        return fila.format(self.nombreplatillo_kc, self.descripcionplatillo_kc, self.precioplatillo_kc)


class Detalle(models.Model):
    iddetalle_kc = models.AutoField(primary_key=True)
    descripciondetalle_kc = models.TextField()
    cantidaddetalle_kc = models.IntegerField()
    pedido = models.ForeignKey(Pedido, null=True, blank=True, on_delete=models.CASCADE)
    platillo = models.ForeignKey(Platillo, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        fila = "{0}: {1}"
        return fila.format(self.descripciondetalle_kc, self.cantidaddetalle_kc)


class Ingrediente(models.Model):
    idingrediente_kc = models.AutoField(primary_key=True)
    nombreingrediente_kc = models.CharField(max_length=150)
    fechaexpiracioningrediente_kc = models.DateField()
    fotografiaingrediente_kc = models.FileField(upload_to='ingredientes', null=True, blank=True)

    def __str__(self):
        fila = "{0}: {1}"
        return fila.format(self.nombreingrediente_kc, self.fechaexpiracioningrediente_kc)


class Receta(models.Model):
    idreceta_kc = models.AutoField(primary_key=True)
    descripcionreceta_kc = models.TextField()
    dificultadreceta_kc = models.CharField(max_length=150)
    ingrediente = models.ForeignKey(Ingrediente, null=True, blank=True, on_delete=models.PROTECT)
    platillo = models.ForeignKey(Platillo, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        fila = "{0}: {1}"
        return fila.format(self.descripcionreceta_kc, self.dificultadreceta_kc)

