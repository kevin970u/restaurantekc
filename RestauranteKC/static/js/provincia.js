function eliminarProvincia(url){
        iziToast.question({
            timeout: 15000,
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Está seguro de eliminar la provincia seleccionada?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href=url;
                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ]
        });
      }
$(document).ready(function() {
  $("#frm_nuevo_provincia").validate({
           rules:{
              "nombreprovincia_kc":{
                required:true,
                letrasOnly:true
              },
              "codigopostalprovincia_kc":{
                required:true,
                number:true,
                minlength:6,
                maxlength:6
              },
              "regionprovincia_kc":{
                required:true,
                letrasOnly:true
              }
           },
           messages:{
             "nombreprovincia_kc":{
               required:"Debe ingresar el nombre de la provincia",
               letrasOnly: "Solo se permiten letras en este campo"
             },
             "codigopostalprovincia_kc":{
               required:"Debe ingresar el codigo postal de la provincia",
               number:"Debe ingresar solo numeros",
               minlength:"El codigo postal no debe tener menos de 6 digitos",
               maxlength:"El codigo postal no debe tener más de 6 digitos"
             },
             "regionprovincia_kc":{
               required:"Debe ingresar la región, ejemplo: Sierra, Costa, Amazonia o Insular",
               letrasOnly: "Solo se permiten letras en este campo"
             }
           }
         });
  $.validator.addMethod("letrasOnly", function(value, element) {
         return this.optional(element) || /^[a-zA-Z]+$/.test(value);
       }, "Solo se permiten letras en este campo.");
  $('#tbl_provincia').DataTable( {
            "language": {
              "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
           },
          dom: 'Bfrtip',
          buttons: [
              {
                  extend: 'pdfHtml5',
                  messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
              },
              'print',
              'csv'              
          ]
        } );
});