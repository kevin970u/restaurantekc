function eliminarDetalle(url){
        iziToast.question({
            timeout: 15000,
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Está seguro de eliminar el detalle seleccionado?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href=url;
                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ]
        });
      }
$(document).ready(function() {
    $("#frm_nuevo_detalle").validate({
         rules:{
            "idpedido_kc":{
              required:true
            },
            "idplatillo_kc":{
              required:true
            },
            "descripciondetalle_kc":{
              required:true,
              letrasYEspacios:true
            },
            "cantidaddetalle_kc":{
              required:true,
              number:true
            },
         },
         messages:{
           "idpedido_kc":{
             required:"Debe seleccionar un pedido"
           },
           "idplatillo_kc":{
             required:"Debe seleccionar un platillo"
           },
           "descripciondetalle_kc":{
              required:"Debe ingresar la descripcion para el detalle",
              letrasYEspacios: "Solo se permiten letras en este campo"
            },
            "cantidaddetalle_kc":{
              required:"Debe ingresar la cantidad para el detalle",
              number:"Solo se permiten numeros en este campo"
            },
         }
       });
    $.validator.addMethod("letrasOnly", function(value, element) {
         return this.optional(element) || /^[a-zA-Z]+$/.test(value);
       }, "Solo se permiten letras en este campo.");
  $('#tbl_detalle').DataTable( {
            "language": {
              "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
           },
          dom: 'Bfrtip',
          buttons: [
              {
                  extend: 'pdfHtml5',
                  messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
              },
              'print',
              'csv'              
          ]
        } );
});