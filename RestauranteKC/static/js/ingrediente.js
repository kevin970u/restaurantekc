function eliminarIngrediente(url){
        iziToast.question({
            timeout: 15000,
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Está seguro de eliminar el ingrediente seleccionado?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href=url;
                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ]
        });
      }
$(document).ready(function() {
    $("#frm_nuevo_ingrediente").validate({
         rules:{
            "nombreingrediente_kc":{
              required:true,
              letrasYEspacios:true
            },
            "fechaexpiracioningrediente_kc":{
              required:true
            },
            "fotografiaingrediente_kc":{
              required:true
            }
         },
         messages:{
           "nombreingrediente_kc":{
             required:"Debe ingresar el nombre del ingrediente",
             letrasYEspacios: "Solo se permiten letras en este campo"
           },
           "fechaexpiracioningrediente_kc":{
             required:"Debe ingresar fecha de expiración del ingrediente"
           },
           "fotografiaingrediente_kc":{
              required:"Debe seleccionar la fotografia del ingrediente"
            }
         }
       });

       $.validator.addMethod("letrasYEspacios", function (value, element) {
        return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
      }, "Solo se permiten letras y espacios en este campo.");
       $('#tbl_ingrediente').DataTable( {
            "language": {
              "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
           },
          dom: 'Bfrtip',
          buttons: [
              {
                  extend: 'pdfHtml5',
                  messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
              },
              'print',
              'csv'              
          ]
        } );
});