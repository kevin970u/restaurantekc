function eliminarCliente(url){
        iziToast.question({
            timeout: 15000,
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Está seguro de eliminar el cliente seleccionado?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href=url;
                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ]
        });
      }

$(document).ready(function() {
    $("#frm_nuevo_cliente").validate({
         rules:{
            "idprovincia_kc":{
              required:true
            },
            "cedulacliente_kc":{
              required:true,
              number:true,
              minlength:10,
              maxlength:10
            },
            "apellidocliente_kc":{
              required:true,
              letrasYEspacios:true
            },
            "nombrecliente_kc":{
              required:true,
              letrasYEspacios:true
            },
            "fotografiacliente_kc":{
              required:true
            }
         },
         messages:{
           "idprovincia_kc":{
             required:"Debe seleccionar una provincia",
           },
           "cedulacliente_kc":{
             required:"Debe ingresar la cedula del cliente",
             number:"Debe ingresar solo numeros",
             minlength:"La cedula no debe tener menos de 10 digitos",
             maxlength:"La cedula no debe tener más de 10 digitos"
           },
           "apellidocliente_kc":{
             required:"Debe ingresar el apellido del cliente",
             letrasYEspacios: "Solo se permiten letras en este campo"
           },
           "nombrecliente_kc":{
             required:"Debe ingresar el nombre del cliente",
             letrasYEspacios: "Solo se permiten letras en este campo"
           },
           "fotografiacliente_kc":{
              required:"Debe seleccionar la fotografia"
            }
         }
       });

$.validator.addMethod("letrasOnly", function(value, element) {
         return this.optional(element) || /^[a-zA-Z]+$/.test(value);
       }, "Solo se permiten letras en este campo.");
  $('#tbl_cliente').DataTable( {
            "language": {
              "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
           },
          dom: 'Bfrtip',
          buttons: [
              {
                  extend: 'pdfHtml5',
                  messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
              },
              'print',
              'csv'              
          ]
        } );
});