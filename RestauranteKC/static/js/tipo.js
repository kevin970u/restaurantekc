function eliminarTipo(url){
        iziToast.question({
            timeout: 15000,
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Está seguro de eliminar el tipo seleccionado?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href=url;
                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ]
        });
      }
$(document).ready(function() {
    $("#frm_nuevo_tipo").validate({
         rules:{
            "categoriatipo_kc":{
              required:true,
              letrasYEspacios:true
            },
            "descripciontipo_kc":{
              required:true,
              letrasYEspacios:true
            },
            "fotografiatipo_kc":{
              required:true
            }
         },
         messages:{
           "categoriatipo_kc":{
             required:"Debe ingresar la categoria",
             letrasYEspacios: "Solo se permiten letras en este campo"
           },
           "descripciontipo_kc":{
             required:"Debe ingresar la descripcion",
             letrasYEspacios: "Solo se permiten letras en este campo"
           },
           "fotografiatipo_kc":{
              required:"Debe seleccionar la fotografia"
            }
         }
       });
    $.validator.addMethod("letrasOnly", function(value, element) {
         return this.optional(element) || /^[a-zA-Z]+$/.test(value);
       }, "Solo se permiten letras en este campo.");
  $('#tbl_tipo').DataTable( {
            "language": {
              "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
           },
          dom: 'Bfrtip',
          buttons: [
              {
                  extend: 'pdfHtml5',
                  messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
              },
              'print',
              'csv'              
          ]
        } );
});