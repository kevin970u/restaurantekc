function eliminarPedido(url){
        iziToast.question({
            timeout: 15000,
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Está seguro de eliminar el pedido seleccionado?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href=url;
                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ]
        });
      }
      // Validar que la fecha de emisión no sea futura
      var fechaEmisionInput = document.getElementById("fechaemisionpedido_kc");
      fechaEmisionInput.addEventListener("change", function() {
        var fechaEmision = new Date(fechaEmisionInput.value);
        var fechaActual = new Date();

        if (fechaEmision > fechaActual) {
          alert("La fecha de emisión no puede ser futura");
          fechaEmisionInput.value = "";  // Limpiar el campo
        }
      });
      var fechaEmisionInput = document.getElementById("fechaemisionpedido_kc");
      fechaEmisionInput.addEventListener("change", function() {
        var fechaEmision = new Date(fechaEmisionInput.value);
        var fechaActual = new Date();

        if (fechaEmision > fechaActual) {
          alert("La fecha de emisión no puede ser futura");
          fechaEmisionInput.value = "";  // Limpiar el campo
        }
      });
  $(document).ready(function() {
          $("#frm_nuevo_pedido").validate({
            rules: {
              "idcliente_kc": {
                required: true
              },
              "fechaemisionpedido_kc": {
                required: true,
                date: true  // Asegúrate de que la fecha sea válida
              },
              "fechavencimientopedido_kc": {
                required: true,
                date: true  // Asegúrate de que la fecha sea válida
              }
            },
            messages: {
              "idcliente_kc": {
                required: "Debe seleccionar un cliente"
              },
              "fechaemisionpedido_kc": {
                required: "Debe ingresar la fecha de emisión"
              },
              "fechavencimientopedido_kc": {
                required: "Debe ingresar la fecha de expiración"
              }
            }
          });
        });
  $('#tbl_pedido').DataTable( {
            "language": {
              "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
           },
          dom: 'Bfrtip',
          buttons: [
              {
                  extend: 'pdfHtml5',
                  messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
              },
              'print',
              'csv'              
          ]
        } );