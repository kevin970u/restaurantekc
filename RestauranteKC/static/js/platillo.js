function eliminarPlatillo(url){
        iziToast.question({
            timeout: 15000,
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Está seguro de eliminar el platillo seleccionado?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href=url;
                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ]
        });
      }
$(document).ready(function() {
    $("#frm_nuevo_platillo").validate({
         rules:{
            "idplatillo_kc":{
              required:true
            },
            "nombreplatillo_kc":{
              required:true,
              letrasYEspacios:true
            },
            "descripcionplatillo_kc":{
              required:true,
              letrasYEspacios:true
            },
            "precioplatillo_kc":{
              required:true,
              number:true
            },
            "fotografiaplatillo_kc":{
              required:true
            },
         },
         messages:{
           "idplatillo_kc":{
             required:"Debe seleccionar un platillo"
           },
           "nombreplatillo_kc":{
             required:"Debe ingresar el nombre de un platillo",
             letrasYEspacios: "Solo se permiten letras en este campo"
           },
           "descripcionplatillo_kc":{
              required:"Debe ingresar la descripcion del platillo",
              letrasYEspacios: "Solo se permiten letras en este campo"
            },
            "descripcionplatillo_kc":{
              required:"Debe ingresar la descripcion del platillo",
              letrasYEspacios: "Solo se permiten letras en este campo"
            },
            "precioplatillo_kc":{
              required:"Debe ingresar el precio del platillo",
              number: "Solo se permiten numeros en este campo"
            },
            "fotografiaplatillo_kc":{
              required:"Debe seleccionar la fotografia del platillo"
            },
         }
       });
    $.validator.addMethod("letrasOnly", function(value, element) {
         return this.optional(element) || /^[a-zA-Z]+$/.test(value);
       }, "Solo se permiten letras en este campo.");
  $('#tbl_platillo').DataTable( {
            "language": {
              "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
           },
          dom: 'Bfrtip',
          buttons: [
              {
                  extend: 'pdfHtml5',
                  messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
              },
              'print',
              'csv'              
          ]
        } );
});