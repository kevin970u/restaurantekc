function eliminarReceta(url){
        iziToast.question({
            timeout: 15000,
            close: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACIÓN',
            message: '¿Está seguro de eliminar la receta seleccionada?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href=url;
                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                }],
            ]
        });
      }
$(document).ready(function() {
    $("#frm_nuevo_receta").validate({
        rules: {
          "idingrediente_kc": {
            required: true
          },
          "idplatillo_kc": {
            required: true
          },
          "descripcionreceta_kc": {
            required: true,
            letrasYEspacios: true
          },
          "dificultadreceta_kc": {
            required: true,
            letrasYEspacios: true
          },
        },
        messages: {
          "idingrediente_kc": {
            required: "Debe seleccionar un ingrediente"
          },
          "idplatillo_kc": {
            required: "Debe seleccionar un platillo"
          },
          "descripcionreceta_kc": {
            required: "Debe ingresar la descripción para la receta",
            letrasYEspacios: "Solo se permiten letras y espacios en este campo"
          },
          "dificultadreceta_kc": {
            required: "Debe ingresar la dificultad de la receta, ejemplo: Facil, Medio, Dificil",
            letrasYEspacios: "Solo se permiten letras y espacios en este campo"
          },
        }
      });

      $.validator.addMethod("letrasYEspacios", function (value, element) {
        return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
      }, "Solo se permiten letras y espacios en este campo.");
      $('#tbl_receta').DataTable( {
            "language": {
              "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
           },
          dom: 'Bfrtip',
          buttons: [
              {
                  extend: 'pdfHtml5',
                  messageTop: 'PDF created by PDFMake with Buttons for DataTables.'
              },
              'print',
              'csv'              
          ]
        } );
});